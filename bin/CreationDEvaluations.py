# Modify Actiona script to adapt click coordinates to different screen resolutions
import re
import argparse
import os


import argparse
parser = argparse.ArgumentParser(description="Usage example: python3 CreationDEvaluations.py --inputFilename mult_div_1_chiffre_01.odt --configFilename eval_math_mult_div_1_chiffre_01.txt")
parser.add_argument("--inputFilename", required=True, help = "Fichier modèle libreoffice writer.")
parser.add_argument("--outputFilename", required=None, help = "Fichier libreoffice writer modifié.")
parser.add_argument("--configFilename", required=True, help = "Fichier de configuration contenant le champ à remplacer et son remplacement (champ à remplacer,champ remplaçant).")
parser.print_help()
args = parser.parse_args()


replacementDict = {}

def readConfigFile(configFilename):
    configFileFieldsArray = {}
    file1 = open("../configFiles/"+configFilename, 'r')
    Lines = file1.readlines()
    file1.close()

    # create replacement dictionnary
    for line in Lines:
        line = line.strip('\n')
        lineElements = line.split(",")
        if (len(lineElements) != 2):
            print("Line : \"" + line + "\" is invalid.")
        else:
            print(line)
            configFileFieldsArray[lineElements[0]] = lineElements[1]

    return configFileFieldsArray


print('inputFilename:', args.inputFilename)

inputFilename = args.inputFilename
configFilename = args.configFilename

outputFilename = ""
if not args.outputFilename:
    outputFilename = os.path.splitext(inputFilename)[0] + "_modified" + os.path.splitext(inputFilename)[1]
else:
    outputFilename = args.outputFilename

    
# get fields to replace infos from configFilename file
if os.path.isfile("../configFiles/" + configFilename):
    replacementDict = readConfigFile(configFilename)
    print(replacementDict)
else:
    print("Le fichier de configuration " + configFilename + " n'a pas été trouvé.")


# unzip odt file into temp directory
if os.path.isfile("../input/" + inputFilename):
    #os.system("unzip ../input/" + inputFilename + " -d "  + "../temp/" + outputFilename)
    os.system("7z x ../input/" + inputFilename + " -o" + "../temp/" + outputFilename + " -r")
else:
    print("Le fichier " + inputFilename + " n'a pas été trouvé.")

# modify content.xml which contains the odt text
for key in replacementDict:
    print(key + "->" + replacementDict[key])
    os.system("sed -i \'s/" + key + "/" + replacementDict[key] + "/g\' ../temp/" + outputFilename + "/content.xml")

# zip the odt uncompressed files and write the result to output dir
os.system("7z a ../ouput/"+ outputFilename + " ../temp/" + outputFilename + "/*")

# delete temp content
os.system("rm -r ../temp/" + outputFilename)








    
    

     





    













 
